
public class GewichtMeetPunt {

    private String datum;
    private String tijd;
    private double gewicht;

    public GewichtMeetPunt(String datum, String tijd, double gewicht) {
            this.datum = datum;
            this.tijd =  tijd;
            this.gewicht = gewicht;

    }

    public String getDatum() {
        return datum;
    }

    public String getTijd() {
        return tijd;
    }

    public double getGewicht() {
        return gewicht;
    }

}
