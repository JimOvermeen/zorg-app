import javax.swing.*;

public class Program {

    private static ZorgGUI zorgGUI;
    private static ZorgGUIEN zorgGUIEN;

    public static void main(String[] args) {
        JFrame TaalVraag = new JFrame();
        JPanel TaalVraagPanel = new JPanel();
        JButton EN = new JButton("English");
        JButton NL = new JButton("Nederlands");
        JLabel Tekst = new JLabel("Kies uw taal. Choose your language.");
        EN.addActionListener(e -> {
            zorgGUIEN = new ZorgGUIEN();
            TaalVraag.dispose();
        });
        NL.addActionListener(e -> {
            zorgGUI = new ZorgGUI();
            TaalVraag.dispose();
        });

        EN.setBounds(100, 100, 100, 40);
        NL.setBounds(120, 100, 100, 40);

        TaalVraag.setSize(300, 200);
        TaalVraagPanel.add(Tekst);
        TaalVraagPanel.add(NL);
        TaalVraagPanel.add(EN);
        TaalVraag.add(TaalVraagPanel);
        TaalVraag.setVisible(true);
    }
}
