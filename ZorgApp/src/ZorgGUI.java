import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.util.ArrayList;

public class ZorgGUI {
    public static JFrame ZorgGUI;
    public static JLabel Welcome;
    public static JButton ButtonZorg;
    public static JButton ButtonPat;

    JSplitPane splitPane = new JSplitPane();

    JLabel naamLabel = new JLabel();
    JLabel leeftijdLabel = new JLabel();
    JLabel gewichtLabel = new JLabel();
    JLabel lengteLabel = new JLabel();
    JLabel bmiLabel = new JLabel();
    JLabel bmiFBLabel = new JLabel();
    JLabel medicijnLabel = new JLabel();

    JPanel panel = new JPanel();

    private Profile profile;

    ArrayList<Profile> profileArrayList = new ArrayList<>();

    JList<Profile> list = new JList<>();
    DefaultListModel<Profile> model = new DefaultListModel<>();

    ArrayList<Medicijn> medicijnLijst = new ArrayList<>();

    private static GewichtMeetPunt gewichtmeetpunt;
    static ArrayList<GewichtMeetPunt> gewichtMeetPuntData = new ArrayList<>();

    public ZorgGUI() {

        Medicijn medicijn = new Medicijn("Paracetamol", "Paracetamol is een pijnstiller. Het verlaagt ook koorts.", "Pijnstiller", "dosering is 10-15 mg paracetamol per kg lichaamsgewicht als een enkele dosis tot een dagelijkse dosis van 60 mg/kg lichaamsgewicht waarbij 3000 mg paracetamol niet overschreden mag worden.");
        medicijnLijst.add(medicijn);
        medicijn = new Medicijn("Aspirine", "Aspirine is een pijnstiller. Het verlaagt ook koorts.", "Pijnstiller", "1 tot 2 tabletten per keer, met een maximum van 8 tabletten per 24 uur");
        medicijnLijst.add(medicijn);
        medicijn = new Medicijn("Penicilline", "Penicilline is een medicijn die door bacteriën veroorzaakte infecties helpen genezen.", "Antibiotica", "De maximale aanbevolen dosis is 100 mg per kilogram lichaamsgewicht per dag.");
        medicijnLijst.add(medicijn);

        profile = new Profile("Jim", "Overmeen", 21, 80, 180);
        profile.profileMedicijnLijst.add(medicijnLijst.get(2));
        profileArrayList.add(profile);
        model.addElement(profile);
        profile = new Profile("Jan", "Janssen", 40, 105, 187);
        model.addElement(profile);
        profile.profileMedicijnLijst.add(medicijnLijst.get(1));
        profileArrayList.add(profile);
        profile = new Profile("Piet", "Vries", 34, 95, 192);
        model.addElement(profile);
        profile.profileMedicijnLijst.add(medicijnLijst.get(0));
        profileArrayList.add(profile);

        ZorgGUI = new JFrame("Zorg App");
        ZorgGUI.setSize(600, 600);

        Welcome = new JLabel("Welkom, bent u een zorgverlener of een patient?");
        Welcome.setBounds(100, 50, 500, 40);
        ZorgGUI.add(Welcome);

        ButtonZorg = new JButton("Zorgverlener");
        ButtonZorg.setBounds(120, 100, 100, 40);
        ZorgGUI.add(ButtonZorg);
        ButtonZorg.addActionListener(e -> ZorgVerlener());

        ButtonPat = new JButton("Patient");
        ButtonPat.setBounds(220, 100, 100, 40);
        ButtonPat.addActionListener(e -> Patient());

        ZorgGUI.add(ButtonPat);

        ZorgGUI.setResizable(false);
        ZorgGUI.setLayout(null);
        ZorgGUI.setVisible(true);
    }

    public void ZorgVerlener() {

        Welcome.setText("Welkom zorgverlener, hier is uw lijst met patiënten.");

        list.setModel(model);
        list.getSelectionModel().addListSelectionListener(e -> {
            Profile p = list.getSelectedValue();

            naamLabel.setText("Naam : " + p.getVoorNaam() + " " + p.getAchterNaam());
            leeftijdLabel.setText("Leeftijd : " + p.getLeeftijd());
            gewichtLabel.setText("Gewicht: " + p.getGewicht());
            lengteLabel.setText("Lengte : " + p.getLengte());
            bmiLabel.setText("BMI : " + p.getBmi(p.getGewicht(), p.getLengte()));
            bmiFBLabel.setText("Feedback : " + p.getBmiFeedBack(p.getBmi(p.getGewicht(), p.getLengte())));
            medicijnLabel.setText("Medicijnen :  " + p.profileMedicijnLijst.get(0).getMedicijnNaam());
        });

        Box box = Box.createVerticalBox();

        JButton ChangeProfile = new JButton("Verander Profiel");
        ChangeProfile.addActionListener(e -> {
            Box box1 = Box.createVerticalBox();

            JFrame ProfileChanger = new JFrame();
            JPanel ProfilePanel = new JPanel();
            JButton Save = new JButton("Opslaan");

            ProfileChanger.setSize(300, 300);

            String[] naam = naamLabel.getText().split(": ");
            String[] naamSplit = naam[1].split(" ");
            String[] leeftijd = leeftijdLabel.getText().split(": ");
            String[] gewicht = gewichtLabel.getText().split(": ");
            String[] lengte = lengteLabel.getText().split(": ");

            JTextField voornaamField = new JTextField(naamSplit[0]);
            JTextField achternaamField = new JTextField(naamSplit[1]);
            JTextField leeftijdField = new JTextField(leeftijd[1]);
            JTextField gewichtField = new JTextField(gewicht[1]);
            JTextField lengteField = new JTextField(lengte[1]);

            Save.addActionListener(e1 -> {
                int Index = list.getSelectedIndex();

                model.get(Index).setVoorNaam(voornaamField.getText());
                model.get(Index).setAchterNaam(achternaamField.getText());
                model.get(Index).setLeeftijd(Integer.parseInt(leeftijdField.getText()));
                model.get(Index).setGewicht(Double.parseDouble(gewichtField.getText()));
                model.get(Index).setLengte(Double.parseDouble(lengteField.getText()));

                ProfileChanger.dispose();

            });

            box1.add(new JLabel("Voornaam:"));
            box1.add(voornaamField);
            box1.add(new JLabel("Achternaam:"));
            box1.add(achternaamField);
            box1.add(new JLabel("Leeftijd:"));
            box1.add(leeftijdField);
            box1.add(new JLabel("Gewicht:"));
            box1.add(gewichtField);
            box1.add(new JLabel("Lengte:"));
            box1.add(lengteField);
            box1.add(Save);

            ProfilePanel.add(box1);

            ProfileChanger.add(ProfilePanel);

            ProfileChanger.setVisible(true);
            ProfileChanger.setResizable(false);
        });

        JButton showChart = new JButton("Gewicht grafiek");
        showChart.addActionListener(e -> {
            Profile p = list.getSelectedValue();
            drawLine(p);
        });

        splitPane.setLeftComponent(new JScrollPane(list));

        box.add(naamLabel);
        box.add(leeftijdLabel);
        box.add(gewichtLabel);
        box.add(lengteLabel);
        box.add(bmiLabel);
        box.add(bmiFBLabel);
        box.add(medicijnLabel);
        box.add(ChangeProfile);
        box.add(showChart);

        panel.add(box);

        splitPane.setRightComponent(panel);

        splitPane.setBounds(60, 100, 500, 400);
        ZorgGUI.add(splitPane);

        ButtonPat.setVisible(false);
        ButtonZorg.setVisible(false);
    }

    public void Patient() {
        Welcome.setText("Welkom patient, vul uw gegevens in");
        Box box = Box.createVerticalBox();

        JPanel loginPanel = new JPanel();
        JPanel profilePanel = new JPanel();
        JLabel userLabel = new JLabel("Gebruikersnaam");
        JLabel passwordLabel = new JLabel("Wachtwoord");
        JTextField userText = new JTextField();
        JPasswordField passwordText = new JPasswordField();
        JButton loginButton = new JButton("Login");
        JLabel loginFout = new JLabel();

        loginButton.addActionListener(e -> {

            String user = userText.getText();
            String password = passwordText.getText();
            int index;
            if (user.equals("Jim") && password.equals("test")) {
                index = 0;
                Welcome.setText("Welkom " + user + ", dit zijn uw gegevens");
                Box box1 = Box.createVerticalBox();

                loginPanel.setVisible(false);
                JLabel NaamLabel = new JLabel("Naam : " + model.get(index).getVoorNaam() + " " + model.get(index).getAchterNaam());
                JLabel LeeftijdLabel = new JLabel("Leeftijd : " + model.get(index).getLeeftijd());
                JLabel GewichtLabel = new JLabel("Gewicht : " + model.get(index).getGewicht());
                JLabel LengteLabel = new JLabel("Lengte : " + model.get(index).getLengte());
                box1.add(NaamLabel);
                box1.add(LeeftijdLabel);
                box1.add(GewichtLabel);
                box1.add(LengteLabel);
                box1.add(new JLabel("BMI : " + model.get(index).getBmi(model.get(index).getGewicht(), model.get(index).getLengte())));
                box1.add(new JLabel("Feedback : " + model.get(index).getBmiFeedBack(model.get(index).getBmi(model.get(index).getGewicht(), model.get(index).getLengte()))));
                box1.add(new JLabel("Medicijnen : " + model.get(index).profileMedicijnLijst.get(0).getMedicijnNaam()));
                JButton changeProfile = new JButton("Verander profiel");
                JButton medicijnInfo = new JButton("Medicijn info");
                JButton drawChart = new JButton("Gewicht grafiek");
                int finalIndex = index;
                drawChart.addActionListener(e3 -> {
                    drawLine(model.get(finalIndex));
                });

                medicijnInfo.addActionListener(e2 -> {
                    Box box2 = Box.createVerticalBox();
                    JFrame MedicijnInfo = new JFrame();
                    box2.add(new JLabel("Medicijn naam : " + model.get(0).profileMedicijnLijst.get(0).getMedicijnNaam()));
                    box2.add(new JLabel("Soort : " + model.get(0).profileMedicijnLijst.get(0).getSoort()));
                    box2.add(new JLabel("Omschrijving : " + model.get(0).profileMedicijnLijst.get(0).getOmschrijving()));
                    box2.add(new JLabel("Dosering : " + model.get(0).profileMedicijnLijst.get(0).getDosering()));
                    box2.setBorder(new EmptyBorder(20, 20, 0, 0));
                    MedicijnInfo.add(box2);
                    MedicijnInfo.setSize(700, 200);
                    MedicijnInfo.setVisible(true);
                });

                int i = index;
                changeProfile.addActionListener(e2 -> {
                    Box box2 = Box.createVerticalBox();
                    JFrame ChangeProfile = new JFrame();
                    JPanel ProfilePanel = new JPanel();
                    ChangeProfile.setSize(300, 300);
                    ChangeProfile.setVisible(true);
                    JTextField VoorNaamChange = new JTextField(model.get(i).getVoorNaam());
                    JTextField AchterNaamChange = new JTextField(model.get(i).getAchterNaam());
                    JTextField LeeftijdChange = new JTextField(String.valueOf(model.get(i).getLeeftijd()));
                    JTextField GewichtChange = new JTextField(String.valueOf(model.get(i).getGewicht()));
                    JTextField LengteChange = new JTextField(String.valueOf(model.get(i).getLengte()));
                    JButton Save = new JButton("Opslaan");
                    Save.addActionListener(e1 -> {
                        NaamLabel.setText("Naam : " + VoorNaamChange.getText() + " " + AchterNaamChange.getText());
                        LeeftijdLabel.setText("Leeftijd : " + LeeftijdChange.getText());
                        GewichtLabel.setText("Gewicht : " + GewichtChange.getText());
                        LengteLabel.setText("Lengte : " + LengteChange.getText());
                        ChangeProfile.dispose();
                    });

                    box2.add(new JLabel("Voornaam:"));
                    box2.add(VoorNaamChange);
                    box2.add(new JLabel("Achternaam:"));
                    box2.add(AchterNaamChange);
                    box2.add(new JLabel("Leeftijd:"));
                    box2.add(LeeftijdChange);
                    box2.add(new JLabel("Gewicht:"));
                    box2.add(GewichtChange);
                    box2.add(new JLabel("Lengte:"));
                    box2.add(LengteChange);
                    box2.add(Save);

                    ProfilePanel.add(box2);
                    ChangeProfile.add(ProfilePanel);

                });

                box1.add(changeProfile);
                box1.add(medicijnInfo);
                box1.add(drawChart);

                profilePanel.add(box1);
                profilePanel.setVisible(true);


            }
            if (user.equals("Jan") && password.equals("test")) {
                index = 1;
                Welcome.setText("Welkom " + user + ", dit zijn uw gegevens");
                Box box1 = Box.createVerticalBox();

                loginPanel.setVisible(false);
                JLabel NaamLabel = new JLabel("Naam : " + model.get(index).getVoorNaam() + " " + model.get(index).getAchterNaam());
                JLabel LeeftijdLabel = new JLabel("Leeftijd : " + model.get(index).getLeeftijd());
                JLabel GewichtLabel = new JLabel("Gewicht : " + model.get(index).getGewicht());
                JLabel LengteLabel = new JLabel("Lengte : " + model.get(index).getLengte());
                box1.add(NaamLabel);
                box1.add(LeeftijdLabel);
                box1.add(GewichtLabel);
                box1.add(LengteLabel);
                box1.add(new JLabel("BMI : " + model.get(index).getBmi(model.get(index).getGewicht(), model.get(index).getLengte())));
                box1.add(new JLabel("Feedback : " + model.get(index).getBmiFeedBack(model.get(index).getBmi(model.get(index).getGewicht(), model.get(index).getLengte()))));
                box1.add(new JLabel("Medicijnen : " + medicijnLijst.get(index).getMedicijnNaam()));
                JButton changeProfile = new JButton("Verander profiel");
                JButton medicijnInfo = new JButton("Medicijn info");

                medicijnInfo.addActionListener(e2 -> {
                    Box box2 = Box.createVerticalBox();
                    JFrame MedicijnInfo = new JFrame();
                    box2.add(new JLabel("Medicijn naam : " + medicijnLijst.get(1).getMedicijnNaam()));
                    box2.add(new JLabel("Soort : " + medicijnLijst.get(1).getSoort()));
                    box2.add(new JLabel("Omschrijving : " + medicijnLijst.get(1).getOmschrijving()));
                    box2.add(new JLabel("Dosering : " + medicijnLijst.get(1).getDosering()));
                    MedicijnInfo.add(box2);
                    MedicijnInfo.setSize(600, 300);
                    MedicijnInfo.setVisible(true);
                });

                int i = index;
                changeProfile.addActionListener(e2 -> {
                    Box box2 = Box.createVerticalBox();
                    JFrame ChangeProfile = new JFrame();
                    JPanel ProfilePanel = new JPanel();
                    ChangeProfile.setSize(300, 300);
                    ChangeProfile.setVisible(true);
                    JTextField VoorNaamChange = new JTextField(model.get(i).getVoorNaam());
                    JTextField AchterNaamChange = new JTextField(model.get(i).getAchterNaam());
                    JTextField LeeftijdChange = new JTextField(String.valueOf(model.get(i).getLeeftijd()));
                    JTextField GewichtChange = new JTextField(String.valueOf(model.get(i).getGewicht()));
                    JTextField LengteChange = new JTextField(String.valueOf(model.get(i).getLengte()));
                    JButton Save = new JButton("Opslaan");

                    box2.add(new JLabel("Voornaam:"));
                    box2.add(VoorNaamChange);
                    box2.add(new JLabel("Achternaam:"));
                    box2.add(AchterNaamChange);
                    box2.add(new JLabel("Leeftijd:"));
                    box2.add(LeeftijdChange);
                    box2.add(new JLabel("Gewicht:"));
                    box2.add(GewichtChange);
                    box2.add(new JLabel("Lengte:"));
                    box2.add(LengteChange);
                    box2.add(Save);

                    Save.addActionListener(e1 -> {
                        NaamLabel.setText("Naam : " + VoorNaamChange.getText() + " " + AchterNaamChange.getText());
                        LeeftijdLabel.setText("Leeftijd : " + LeeftijdChange.getText());
                        GewichtLabel.setText("Gewicht : " + GewichtChange.getText());
                        LengteLabel.setText("Lengte : " + LengteChange.getText());
                        ChangeProfile.dispose();
                    });

                    ProfilePanel.add(box2);
                    ChangeProfile.add(ProfilePanel);

                });
                box1.add(changeProfile);
                box1.add(medicijnInfo);

                profilePanel.add(box1);
                profilePanel.setVisible(true);
            } else {
                loginFout.setText("Login mislukt");
            }

        });

        box.add(userLabel);
        box.add(userText);
        box.add(passwordLabel);
        box.add(passwordText);
        box.add(loginButton);
        box.add(loginFout);

        loginPanel.add(box);
        loginPanel.setBounds(60, 100, 500, 400);

        profilePanel.setBounds(60, 100, 500, 400);
        profilePanel.setVisible(false);

        ZorgGUI.add(profilePanel);
        ZorgGUI.add(loginPanel);

        ButtonPat.setVisible(false);
        ButtonZorg.setVisible(false);

    }

    public void drawLine(Profile p) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("Gewicht grafiek van " + p.getVoorNaam() + " " + p.getAchterNaam());
            String grafiekName = "Gewicht grafiek van " + p.getVoorNaam() + " " + p.getAchterNaam();
            frame.setSize(800, 400);
            frame.setVisible(true);

            DefaultCategoryDataset ds = createDataset(p);
            JFreeChart chart = ChartFactory.createLineChart(grafiekName, "Datum", "Gewicht (kg)", ds, PlotOrientation.VERTICAL, true, true, false);
            ChartPanel cp = new ChartPanel(chart);

            frame.getContentPane().add(cp);
        });
    }

    private DefaultCategoryDataset createDataset(Profile p) {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        if (p.getVoorNaam().equals("Jim")) {
            gewichtMeetPuntData.clear();
            gewichtmeetpunt = new GewichtMeetPunt("19 mei 2020", "14:15", 80);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("29 mei 2020", "14:15", 82);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("12 juni 2020", "14:15", 83);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("25 juni 2020", "14:15", 79);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("6 juli 2020", "14:15", 78);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("9 juli 2020", "14:15", 80);
            gewichtMeetPuntData.add(gewichtmeetpunt);
        }
        if (p.getVoorNaam().equals("Jan")) {
            gewichtMeetPuntData.clear();
            gewichtmeetpunt = new GewichtMeetPunt("15 mei 2020", "14:15", 90);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("30 mei 2020", "14:15", 92);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("19 juni 2020", "14:15", 95);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("24 juni 2020", "14:15", 100);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("19 juli 2020", "14:15", 92);
            gewichtMeetPuntData.add(gewichtmeetpunt);
        }
        if (p.getVoorNaam().equals("Piet")) {
            gewichtMeetPuntData.clear();
            gewichtmeetpunt = new GewichtMeetPunt("9 aug 2020", "14:15", 70);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("20 aug 2020", "14:15", 80);
            gewichtMeetPuntData.add(gewichtmeetpunt);
            gewichtmeetpunt = new GewichtMeetPunt("28 aug 2020", "14:15", 90);
            gewichtMeetPuntData.add(gewichtmeetpunt);
        }

        for (int i = 0; i < gewichtMeetPuntData.toArray().length; i++) {
            dataset.addValue(gewichtMeetPuntData.get(i).getGewicht(), "Gewicht", gewichtMeetPuntData.get(i).getDatum());
        }

        return dataset;

    }


}