import java.util.ArrayList;
import java.util.Scanner;

// niet meer in gebruik
public class ZorgApp {
	
	private profileList ProfileList;
	
	private Profile profile;
	private Profile profile2;
	
	private Medicijn medicijn;
	ArrayList<Medicijn> medicijnLijst = new ArrayList<Medicijn>();
	
	public ZorgApp() {
						
		profile = new Profile("Jim", "Overmeen",21, 80, 180);
		ProfileList.add(profile);
		
		profile2 = new Profile("Jan", "Janssen", 40, 85, 187);
		ProfileList.add(profile);
		
		medicijn = new Medicijn("Paracetamol", "Paracetamol is een pijnstiller. Het verlaagt ook koorts.", "Pijnstiller", "dosering is 10-15 mg paracetamol per kg lichaamsgewicht als een enkele dosis tot een dagelijkse dosis van 60 mg/kg lichaamsgewicht waarbij 3000 mg paracetamol niet overschreden mag worden." );
		medicijnLijst.add(medicijn);

		medicijn = new Medicijn("Asprine", "Asprine is een pijnstiller. Het verlaagt ook koorts.", "Pijnstiller", "1 tot 2 tabletten per keer, met een maximum van 8 tabletten per 24 uur" );
		medicijnLijst.add(medicijn);
		
		medicijn = new Medicijn("Penicilline", "Penicilline is een medicijn die door bacteriën veroorzaakte infecties helpen genezen.", "Antibiotica", "De maximale aanbevolen dosis is 100 mg per kilogram lichaamsgewicht per dag." );
		medicijnLijst.add(medicijn);

				
		Scanner AskVoorNaam = new Scanner(System.in);
		System.out.println("Voornaam?");
		String AskedVoorNaam = AskVoorNaam.nextLine();
		
		if(AskedVoorNaam.equals("Jim")) {
			System.out.println("Naam : " + profile.getVoorNaam() + " " + profile.getAchterNaam() + "\nLeeftijd : " + profile.getLeeftijd() + " jaar");
			System.out.println("Gewicht : "+ profile.getGewicht() + " kg" + "\nLengte : "+ profile.getLengte() + " cm");
			System.out.println("Bmi : "+ profile.getBmi(profile.getGewicht(), profile.getLengte())+ " kg/m2");
			System.out.println("Medicijnlijst : "+ medicijnLijst.get(2).getMedicijnNaam());
		} if (AskedVoorNaam.equals("Jan")) {
			System.out.println("Naam : " + profile2.getVoorNaam() + " " + profile2.getAchterNaam() + "\nLeeftijd : " + profile2.getLeeftijd() + " jaar");
			System.out.println("Gewicht : "+ profile2.getGewicht() + " kg" + "\nLengte : "+ profile2.getLengte()+ " cm");
			System.out.println("Bmi : "+ profile2.getBmi(profile2.getGewicht(), profile2.getLengte()) + " kg/m2");
			System.out.println("Medicijnlijst : "+ medicijnLijst.get(1).getMedicijnNaam() + ", " + medicijnLijst.get(0).getMedicijnNaam() );
		} if(!AskedVoorNaam.equals("Jim") && !AskedVoorNaam.equals("Jan")) {
			System.out.println("Ingevulde naam is niet gevonden.");
			 System.exit(1);
		}
		
	}
		
	public void ProfileChanger() {
		
		Scanner ChangeVoorNaam = new Scanner(System.in);
		System.out.println("Vul uw voornaam in: ");
		String ChangedVoorNaam = ChangeVoorNaam.nextLine();
		
		Scanner ChangeAchterNaam = new Scanner(System.in);
		System.out.println("Vul uw achternaam in: ");
		String ChangedAchterNaam = ChangeAchterNaam.nextLine();
		
		Scanner ChangeLeeftijd = new Scanner(System.in);
		System.out.println("Vul uw leeftijd in: ");
		int ChangedLeeftijd = ChangeLeeftijd.nextInt();
		
		Scanner ChangeGewicht = new Scanner(System.in);
		System.out.println("Vul uw gewicht in: ");
		int ChangedGewicht = ChangeGewicht.nextInt();
		
		Scanner ChangeLengte = new Scanner(System.in);
		System.out.println("Vul uw lengte in (in cm): ");
		int ChangedLengte = ChangeLengte.nextInt();
		
		profile = new Profile(ChangedVoorNaam, ChangedAchterNaam, ChangedLeeftijd, ChangedGewicht, ChangedLengte);
		
		System.out.println("\nGegevens zijn veranderd naar \nNaam : " + profile.getVoorNaam() + " " + profile.getAchterNaam() + "\nLeeftijd : " + profile.getLeeftijd());
		System.out.println("Gewicht : "+ profile.getGewicht() + " kg" + "\nLengte : "+ profile.getLengte()+ " cm");
		System.out.println("Bmi : "+ profile.getBmi(profile.getGewicht(), profile.getLengte())+ " kg/m2");
	}
	
	public void MedicijnChanger() {
		System.out.println("\n");
		for (int i = 0; i < medicijnLijst.size(); i++) {
				int ii = i+1;
				System.out.println(ii + ". "+ medicijnLijst.get(i).getMedicijnNaam());
		    }
		Scanner AddMedicijn = new Scanner(System.in);
		System.out.println("Welke wilt u toevoegen?");
		int AddedMedicijn = AddMedicijn.nextInt();
		
		int index = AddedMedicijn - 1;
		System.out.println("\n"+ medicijnLijst.get(index).getMedicijnNaam() + " is toegevoegd aan de medicijnen lijst");
		
	}
	
	
	}
