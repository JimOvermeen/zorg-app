import java.util.ArrayList;

public class Profile {

    ArrayList<Medicijn> profileMedicijnLijst = new ArrayList<>();
    
    private String voorNaam;
    private String achterNaam;
    private int leeftijd;
    private double gewicht;
    private double lengte;

    public Profile(String voorNaam, String achterNaam, int leeftijd, double gewicht, double lengte) {
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        this.leeftijd = leeftijd;
        this.gewicht = gewicht;
        this.lengte = lengte;
    }


    public String getVoorNaam() {
        return voorNaam;
    }

    public void setVoorNaam(String voorNaam) {
        this.voorNaam = voorNaam;
    }

    public String getAchterNaam() {
        return achterNaam;
    }

    public void setAchterNaam(String achterNaam) {
        this.achterNaam = achterNaam;
    }

    public int getLeeftijd() {
        return leeftijd;
    }

    public void setLeeftijd(int leeftijd) {
        this.leeftijd = leeftijd;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    public void setLengte(double lengte) {
        this.lengte = lengte;
    }

    public double getLengte() {
        return lengte;
    }

    public double getBmi(double gewicht, double lengte) {

        double lengteMeters = lengte / 100;

        double Bmi = gewicht / (lengteMeters * lengteMeters);

        return Math.round(Bmi * 100.0) / 100.0;

    }

    public String getBmiFeedBack(double Bmi) {

        String bmiFeedBack;

        if (Bmi < 18.5) {
            bmiFeedBack = "Ondergewicht";
        } else if (Bmi < 25) {
            bmiFeedBack = "Gezond gewicht";
        } else if (Bmi < 30) {
            bmiFeedBack = "Overgewicht";
        } else {
            bmiFeedBack = "Ernstig overgewicht (obesitas)";
        }

        return bmiFeedBack;
    }

    @Override
    public String toString() {
        String voornaam = getVoorNaam();
        String achternaam = getAchterNaam();

        return voornaam + " " + achternaam;
    }


}
